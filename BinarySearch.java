import java.util.Scanner;

public class BinarySearch {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int arr[] = {1, 5, 7, 9, 15, 19, 25, 53, 79, 98};
        int n = scan.nextInt();
        int binarySearch = binarySearch(arr,n,0, arr.length-1);
        System.out.println(binarySearch);
    }
    public static int binarySearch(int [] arr, int n, int first, int last) {
        if (first <= last) {
            int mid = first + (last - first) / 2;
            if (arr[mid] == n) {
                return mid ;
            }
            else if (arr[mid] > n ){
                return binarySearch(arr,n,first,mid-1);
            }
            return binarySearch(arr,n,mid+1,last);

        }
        return -1;
    }

}
